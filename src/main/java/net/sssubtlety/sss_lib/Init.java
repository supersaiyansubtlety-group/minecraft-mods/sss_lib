package net.sssubtlety.sss_lib;

import net.fabricmc.api.ModInitializer;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        FeatureControl.init();
    }
}
