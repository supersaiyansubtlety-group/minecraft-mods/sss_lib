package net.sssubtlety.sss_lib;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

import static net.sssubtlety.sss_lib.SssLib.NAMESPACE;

@me.shedaniel.autoconfig.annotation.Config(name = NAMESPACE)
public class Config implements ConfigData {
    @ConfigEntry.Gui.Tooltip()
    boolean fetch_translation_updates = FeatureControl.Defaults.fetch_translation_updates;
}
