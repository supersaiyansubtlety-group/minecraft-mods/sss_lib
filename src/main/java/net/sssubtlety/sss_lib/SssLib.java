package net.sssubtlety.sss_lib;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SssLib {
	public static final String NAMESPACE = "sss_lib";
	public static final Util.TranslatableString NAME = new Util.TranslatableString("text." + NAMESPACE + ".name");
	public static final Logger LOGGER = LogManager.getLogger();
}
