package net.sssubtlety.sss_lib;

import de.guntram.mcmod.crowdintranslate.CrowdinTranslate;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;

import static net.sssubtlety.sss_lib.FeatureControl.shouldFetchTranslationUpdates;

public class ClientInit implements ClientModInitializer {
    static {
        if (shouldFetchTranslationUpdates())
            CrowdinTranslate.downloadTranslations("mod-id", SssLib.NAMESPACE);
    }

    @Override
    @Environment(EnvType.CLIENT)
    public void onInitializeClient() {
    }
}
